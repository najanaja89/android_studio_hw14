package com.example.hw14;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final String DB_NAME = "animals";
    private final String TEXT_KEY = "text key";
    private final int version = 1;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        EditText editText = findViewById(R.id.editText);
//        EditText editTextTable = findViewById(R.id.editTextTable);
        TextInputLayout tableNameLayout = findViewById(R.id.tableNameLayout);
        TextInputLayout tableIdLayout = findViewById(R.id.idNameLayout);
        TextInputLayout tableValueLayout = findViewById(R.id.valueNameLayout);

        TextInputEditText tableName = findViewById(R.id.tableName);
        TextInputEditText id = findViewById(R.id.idName);
        TextInputEditText value = findViewById(R.id.valueName);

        Button buttonSave = findViewById(R.id.btnSave);
        Button buttonLoad = findViewById(R.id.btnLoad);
        Button buttonUpdate = findViewById(R.id.btnUpdate);
        Button buttonDelete = findViewById(R.id.btnDelete);
        Button buttonDeleteTable = findViewById(R.id.btnDeleteTable);
        Button buttonCreateTable = findViewById(R.id.btnCreateTable);
        Button buttonSearch = findViewById(R.id.btnSearch);

        Spinner spinner = findViewById(R.id.spinner);


        MyDatabase databaseHelper = new MyDatabase(MainActivity.this, DB_NAME, null, version);

//        try {
//            database = databaseHelper.getWritableDatabase();
//        } catch (Exception e) {
//            database = databaseHelper.getReadableDatabase();
//        }
//        Log.d("Hello", database.toString());
        //database.close();

        //SQLiteDatabase finalDatabase = database;


        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database = databaseHelper.getReadableDatabase();
                Cursor cursor = database.query(
                        "animals",
                        new String[]{"name"},
                        "name=?",
                        new String[]{value.getText().toString()},
                        "name",
                        null,
                        "name"
                        );
                ArrayList<String> spinnerItems = new ArrayList<>();
                if(cursor.moveToFirst()){
                    int nameIndex = cursor.getColumnIndex("name");
                    do {
                      String name = cursor.getString(nameIndex);
                      spinnerItems.add(name);
                    }
                    while(cursor.moveToNext());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(
                        MainActivity.this,
                        android.R.layout.simple_spinner_item,
                        spinnerItems);
                cursor.close();
                database.close();
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (id.getText().length() == 0 && value.getText().length() == 0) {
                    if (id.getText().length() == 0) {
                        tableIdLayout.setError("Enter id");
                    }
                    if (value.getText().length() == 0) {
                        tableValueLayout.setError("Enter Value");
                    }
                } else {
                    tableIdLayout.setError(null);
                    tableValueLayout.setError(null);
                    database = databaseHelper.getWritableDatabase();
                    int count = database.delete("animals", "id=?", new String[]{id.getText().toString()});
                    Log.d("Hello", "Delete count " + count);
                    database.close();
                }
            }
        });

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (id.getText().length() == 0 && value.getText().length() == 0) {
                    if (id.getText().length() == 0) {
                        tableIdLayout.setError("Enter id");
                    }
                    if (value.getText().length() == 0) {
                        tableValueLayout.setError("Enter Value");
                    }
                } else {
                    tableIdLayout.setError(null);
                    tableValueLayout.setError(null);
                    database = databaseHelper.getWritableDatabase();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("name", value.getText().toString());
                    int count = database.update("animals", contentValues, "id=?", new String[]{id.getText().toString()});
                    Log.d("Hello", "Update count " + count);
                    database.close();
                }
            }
        });
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database = databaseHelper.getWritableDatabase();
                if (value.getText().length() == 0) {
                    tableValueLayout.setError("Enter Value");
                } else {
                    tableValueLayout.setError(null);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("name", value.getText().toString());
                    long rowId = database.insert("animals", null, contentValues);
                    Log.d("Hello", "ROW ID " + rowId);
                    database.close();
                }
            }
        });

        buttonLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database = databaseHelper.getReadableDatabase();
                Cursor cursor = database.query("animals", null, null, null, null, null, null);
                ArrayList<String> spinnerItems = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    int idIndex = cursor.getColumnIndex("id");
                    int nameIndex = cursor.getColumnIndex("name");

                    do {
                        int id = cursor.getInt(idIndex);
                        String name = cursor.getString(nameIndex);

                        spinnerItems.add("Id: " + id + ", Name " + name);
                        Log.d("Hello", "ID " + id + "String  " + name);
                    }
                    while (cursor.moveToNext());
                } else {
                    Log.d("Hello", "Cursor is empty ");
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,
                        android.R.layout.simple_spinner_item,
                        spinnerItems);
                spinner.setAdapter(adapter);
                cursor.close();
                database.close();
            }
        });

        buttonCreateTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tableName.getText().length() == 0) {
                    tableNameLayout.setError("Enter table name");
                } else {
                    tableNameLayout.setError(null);
//                    database = databaseHelper.getWritableDatabase();
//                    database.execSQL("create table " + tableName.getText().toString() +
//                            "(id integer primary key autoincrement, name TEXT)");
//                    database.close();

                }
            }
        });

        buttonDeleteTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tableName.getText().length() == 0) {
                    tableNameLayout.setError("Enter table name");
                } else {
                    tableNameLayout.setError(null);
                    database = databaseHelper.getWritableDatabase();
                    int deleteCount = database.delete(tableName.getText().toString(), null, null);
                    database.close();
                }
            }
        });
    }
}